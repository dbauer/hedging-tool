package edu.columbia.nlp.hedging;

import com.beust.jcommander.Parameter;
import java.util.List;
import java.util.ArrayList;

public class CommandLineParser {

  @Parameter(names = "-model", description = "Alternative Weka model file to use.")
  public String model;
  
  @Parameter(names = "-hwords", description = "Alternative hedge word file to use.")
  public String hwords;
  
  @Parameter(names = "-properties", description = "Java properties file containing configuration.")
  public String properties; 

  @Parameter(description = "List of input files. Read from stdin if none specified.")
  public List<String> inputFiles = new ArrayList<String>();

  @Parameter(names = "--help", help = true)
  private boolean help;

}

