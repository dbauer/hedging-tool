package edu.columbia.nlp.hedging;

/**
 * A stand-allone annotation instance for potential hedge words. 
 * The hedge word predictor collects a list of these annotation objects. The annotations are 
 * merged back into the output when the annotation for the entire sentence is printed.
 */ 
public class HedgeAnnotation {

    public int tokStart;        // token index at which the annotation starts
    public int tokEnd;          // token index at which the annotation ends 
    public HedgeWord hedgeword; 
    public boolean prediction;  // true = this is a hedge word
    public double score;        // confidence score returned by weka

    HedgeAnnotation(int theTokStart, int theTokEnd, HedgeWord theHedgeword, boolean thePrediction, double theScore) {
        tokStart = theTokStart;
        tokEnd = theTokEnd;
        hedgeword = theHedgeword;
        prediction = thePrediction;
        score = theScore;
    }

    /**
     * @return a String representation of this hedge representation that can be used as attributes in an XML tag.
     */
    public String xmlAttributeString() {
        StringBuilder result = new StringBuilder();
        result.append("type=\"");
        result.append(hedgeword.type == HedgeWord.htype.HPROP ? "hProp" : "hRel");
        result.append("\" ");
        result.append("prediction=\"");
        result.append(prediction);
        result.append("\" ");
        result.append("score=\"");
        result.append(String.format("%.4f",score));
        result.append("\"");
        return result.toString();
    }

}
