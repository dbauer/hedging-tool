package edu.columbia.nlp.hedging;

import edu.stanford.nlp.ling.CoreLabel;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

/**
 * Represent an input sentence together with a set of annotation objects. 
 * The annotation objects are kept as stand-alone annotations so that multi-word 
 * hedges can be annotated. 
 */
public class SentenceAndAnnotations { 
    private List<CoreLabel> sentence; 
    private HedgeAnnotation[] annotations;

    /**
     * Construct a new annotation object for this sentence.  
     * @param theSentence the sentence on which hedge words are annotated. This should be an ArrayList. 
     */ 
    public SentenceAndAnnotations(List<CoreLabel> theSentence) { 
        sentence = theSentence;
        annotations = new HedgeAnnotation[sentence.size()];
    }

    /**
     * Add a hedge annotation. 
     * @param annotation the annotation object to add.
     */ 
    public void addAnnotation(HedgeAnnotation annotation) {
        annotations[annotation.tokStart] = annotation;
    }

    public String toString() {
        StringBuilder result = new StringBuilder(512);
        int i = 0;
        while (i < sentence.size()) {
            HedgeAnnotation anno_for_token = annotations[i];
            if (anno_for_token != null) {
                result.append("<hedgeWord ");        
                result.append(anno_for_token.xmlAttributeString());
                result.append("> ");
                while (i <= anno_for_token.tokEnd) { 
                    result.append(sentence.get(i).originalText());
                    result.append(" ");
                    i++;
                }
                result.append("</hedgeWord> ");
            } else {
                result.append(sentence.get(i).originalText());
                result.append(" ");
                i++;
            }  
        } 
        return result.toString();
    }
}
