package edu.columbia.nlp.hedging;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import au.com.bytecode.opencsv.CSVReader;

import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.pipeline.Annotation;

/**
 * HedgePredictor objects own a list of hedge words and a weka classifier and can annotate
 * hedge words on one sentence at a time.
 **/ 
public class HedgePredictor {
    
    private static String DEFAULTHWORDFILE = "/hedgewords.041715.csv";   
    private Map<String, HedgeWord> hedge_word_map;

    WekaClassifier classifier;

    public HedgePredictor(Properties properties) throws Exception{
        String hedgeWordFilename = properties.getProperty("hwordfile", DEFAULTHWORDFILE); 
        loadHedgeWordList(hedgeWordFilename);
        classifier = new WekaClassifier(properties);
    }
    
    /**
     * Process a sentence and return an object containing the sentence plus a list of 
     * standalone hedge annotations. 
     */
    public SentenceAndAnnotations processSentence(List<CoreLabel> original_sent) throws Exception{

        List<CoreLabel> sentence = new ArrayList<CoreLabel>(original_sent); 
        SentenceAndAnnotations result = new SentenceAndAnnotations(sentence);

        // Now look at each possible n-gram in the sentence.
        for (int i=0; i < sentence.size(); i++) {
            for (int n=5; n >= 1; n--) {
                if (i+1 >= n) {
                    StringBuilder ngram_builder = new StringBuilder();
                    for (int j = i-n+1; j<=i; j++) {
                        ngram_builder.append(sentence.get(j).get(CoreAnnotations.TextAnnotation.class));
                        ngram_builder.append(" ");
                    }
                    String ngram = ngram_builder.toString().trim();  // Make a string out of the ngram
                    HedgeWord hedgeword = hedge_word_map.get(ngram); // Check if it is in the dictionary 
                    if (hedgeword != null) {                         // If so, classify it. 
                        result.addAnnotation(classifier.predict(hedgeword, sentence, i, n));
                    }
                }
            }
        }
        return result;
    } 

    /**
     * Load a list of hedge words and their types from a CSV file.
     */
    private void loadHedgeWordList(String hedge_word_file) throws IOException {

        InputStream is = getClass().getResourceAsStream(hedge_word_file); 
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        CSVReader reader = new CSVReader(br);
        hedge_word_map = new HashMap<String, HedgeWord>();
        String [] nextLine;
        int count = 0;
        while ((nextLine = reader.readNext()) != null) {
            String word = nextLine[0].trim().toLowerCase();
            String typestr = nextLine[1].trim();
            HedgeWord.htype type = typestr.equals("hProp") ? HedgeWord.htype.HPROP : HedgeWord.htype.HREL;
            hedge_word_map.put(word, new HedgeWord(word, type, count));
            count += 1;
        }    
    }
} 
