package edu.columbia.nlp.hedging;

import java.io.Reader;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader; 
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations;

import com.beust.jcommander.JCommander;

/**
 * Main application.
 */
public class HedgingTool {

    private HedgePredictor predictor;  // This object annotates all hedge words in a sentence.
    private Preprocessor preprocessor; // The preprocessor for sentence splitting, tokenization, pos tagging
    
    /**
     * Process the text in an input stream by splitting it and print annotated output to stdout. 
     * This will first run the preprocessor to get a list of tokenized and POS tagged sentences 
     * and then annotate and print each sentence at a time.
     * @param document the input document. 
     */
    public void processDocument(InputStream document) throws Exception{
        List<CoreMap> sentences = preprocessor.preprocess(document);  
        for (CoreMap sentence : sentences) {
            SentenceAndAnnotations annotations = predictor.processSentence(sentence.get(CoreAnnotations.TokensAnnotation.class));
            System.out.println(annotations);
        }
    } 
    
    /**
     * Create a new HedgingTool instance that can process one document at a time. 
     */
    public HedgingTool() throws Exception{
        this(new Properties());
    }

    /**
     * Create a new HedgingTool instance that can process one document at a time. 
     * @param properties Configuration for the app.
     */
    public HedgingTool(Properties properties) throws Exception{
        preprocessor = new Preprocessor();
        predictor = new HedgePredictor(properties); 
    }

    public static void main(String[] args) throws Exception{

            CommandLineParser commands = new CommandLineParser();
            new JCommander(commands,args);

            // Try to read a configuration file
            Properties props = new Properties();
            if (commands.properties != null) {
                try {
                    props.load(new BufferedReader(new InputStreamReader(new FileInputStream(commands.properties))));
                } catch (IOException e) {
                    System.err.println("Could not read properties from file ".concat(commands.properties));
                }
            } 

            // Set command line parameters in configuration object
            if (commands.model != null) props.setProperty("wekamodel", commands.model);            
            if (commands.hwords != null) props.setProperty("hwordfile", commands.hwords);            

            // Pass configuration to new instance for the tool.
            HedgingTool tool = new HedgingTool(props);

            if (commands.inputFiles.isEmpty()) {
                tool.processDocument(System.in);    // Process standard input
            } else {
                for (String filename : commands.inputFiles) {   // Process each file.
                    try {
                        InputStream is = new FileInputStream(filename);
                        if (is==null) throw new IOException("Could not open file ".concat(filename));
                        tool.processDocument(is); // Run the hedging tool on each document. 
                    } catch (IOException e) {
                        System.err.println("Could not open file ".concat(filename));
                    }
                }
            }
    }
}
