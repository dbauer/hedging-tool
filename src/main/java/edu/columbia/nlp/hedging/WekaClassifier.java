package edu.columbia.nlp.hedging;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

import edu.stanford.nlp.ling.CoreLabel;

import weka.core.FastVector;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;
import edu.stanford.nlp.ling.CoreAnnotations;
import weka.classifiers.trees.ADTree; 
import weka.classifiers.Classifier;

import java.io.InputStream;
import java.util.Properties;


/**
 * A wrapper around a weka classifier that also includes features extraction. 
 */ 
public class WekaClassifier {
    
    private static String DEFAULTWEKAMODEL = "/ADTreeModel.062015.model";

    Classifier classifier;
    Map<String, Integer> tagToId; 
    Instances dataTemplate;
    double[] zeroes;

    public WekaClassifier(Properties properties) throws Exception{
        
        String wekaModelFilename = properties.getProperty("wekamodel", DEFAULTWEKAMODEL); 

        InputStream is = getClass().getResourceAsStream(wekaModelFilename);
        classifier = (Classifier) SerializationHelper.read(is); // load the WEKA classifier. 

        // tagToID is a dictionary of POS tags to feature indices. 
        String[] taglist = new String[]{"MD","RB","WRB","PRP","VBP","JJ","NNS","VB",
                                        "VBN","IN","VBZ","PDT","DT","NNP","NN","CC",
                                        "JJS","VBD","WP","S","-RRB-","-LRB-","PRP$",
                                        "CD","TO","$","LS","WDT",":",".","JJR","NNPS",
                                        "RP","FW","EX","RBR","RBS","POS","VBG","''"};
        tagToId = new HashMap<String,Integer>();
        for (int i=0; i<taglist.length; i++ ) 
            tagToId.put(taglist[i], i);

        dataTemplate = makeDataTemplate();
        // fill an array with zeroes to quickly create new instance vectors.
        zeroes = new double[dataTemplate.numAttributes()];
    }


    /**
     * Predict if the word in position tokEnd is used as a hedge word. 
     * @param hedgeword The HedgeWord object for which a prediction is being made. 
     * @param sentence The sentence. 
     * @param tokEnd The position of this word. If the classification instance is a multi-word this is the token position of the last token. 
     * @param n The length of the multi-word. 
     * @return a HedgeAnnotation object containing the new prediction and confidence score.
     */
    public HedgeAnnotation predict(HedgeWord hedgeword, List<CoreLabel> sentence, int tokEnd, int n) throws Exception{
        // For now classify all multi words as hedges. 
        if (n>1) return new HedgeAnnotation(tokEnd-n+1, tokEnd, hedgeword, true, 1.0);
        Instance inst = getInstance(hedgeword, sentence, tokEnd, n); 
        // TODO: Handle exception here
        Double dresult = classifier.classifyInstance(inst);
        int result = dresult.intValue();
        double[] distribution = classifier.distributionForInstance(inst);
        boolean prediction = result==0 ? true : false;
        return new HedgeAnnotation(tokEnd, tokEnd, hedgeword, prediction, distribution[result]);
    }

    /**
     * Creates the dataset object required by Weka. We will not really store data in this object, but merely 
     * use it to communicate the layout of instance vectors to the classifier. 
     */
    private Instances makeDataTemplate() {
        FastVector atts = new FastVector();
        // Word feature  
        atts.addElement(new Attribute("word"));
        // Word pos features
        for (int i=0; i<40; i++) atts.addElement(new Attribute("POS_w".concat(Integer.toString(i))));
        // POS feature of word at position -1 
        for (int i=0; i<40; i++) atts.addElement(new Attribute("POS_1".concat(Integer.toString(i))));
        // POS feature of word at position -2 
        for (int i=0; i<40; i++) atts.addElement(new Attribute("POS_2".concat(Integer.toString(i))));
        // POS feature of word at position +1 
        for (int i=0; i<40; i++) atts.addElement(new Attribute("POS1".concat(Integer.toString(i))));
        // POS feature of word at position +2 
        for (int i=0; i<40; i++) atts.addElement(new Attribute("POS2".concat(Integer.toString(i))));

        FastVector values = new FastVector(); 
        values.addElement("n");       
        values.addElement("y");       
        atts.addElement(new Attribute("class", values));
        Instances dataTemplate = new Instances("hedge", atts, 0);
        dataTemplate.setClassIndex(dataTemplate.numAttributes() - 1);
        return dataTemplate;
    }

    /**
     * This is the feature extraction method. 
     * @param hedgeword The HedgeWord object for which a prediction is being made. 
     * @param sentence The sentence. 
     * @param tokEnd The position of this word. If the classification instance is a multi-word this is the token position of the last token. 
     * @param n The length of the multi-word. 
     * @return a Weka feature vector. 
     */
    private Instance getInstance(HedgeWord hedgeword, List<CoreLabel> sentence, int tokEnd, int n) {
        
        Integer posindex;
        
        int tokStart = tokEnd-n+1;  // Start position for multi-words.

        Instance inst = new Instance(1, zeroes);  // Create a new feature vector initialized to 0. 
        
        // Feature 0: Index into the headword table
        inst.setValue(0,hedgeword.id);

        // Feature 1-40: POS of target word at position i  TODO: What do we do with multi words? 
        String pos = sentence.get(tokEnd).get(CoreAnnotations.PartOfSpeechAnnotation.class);
        posindex = tagToId.get(pos);
        if (posindex != null)
            inst.setValue(1+posindex,1.0);
        // Feature 41-80: POS of previous word at position i-1
        if (tokStart>0) {
            String pos_minus_1 = sentence.get(tokStart-1).get(CoreAnnotations.PartOfSpeechAnnotation.class);
            posindex = tagToId.get(pos_minus_1);
            if (posindex != null) 
                inst.setValue(41+posindex,1.0);
        }
        // Feature 81-120: POS of word at position i-2 
        if (tokStart>1) { 
            String pos_minus_2 = sentence.get(tokStart-2).get(CoreAnnotations.PartOfSpeechAnnotation.class);
            posindex = tagToId.get(pos_minus_2);
            if (posindex != null) 
                inst.setValue(81+posindex,1.0);
        }
        // Feature 121-160: POS of word at position i+1 
        if (tokEnd<sentence.size()-1) {
            String pos_plus_1 = sentence.get(tokEnd+1).get(CoreAnnotations.PartOfSpeechAnnotation.class);
            posindex = tagToId.get(pos_plus_1);
            if (posindex != null) 
                inst.setValue(121+posindex,1.0);
        }
        // Feature 161-200: POS of word at position i+2 
        if (tokEnd<sentence.size()-2) {
            String pos_plus_2 = sentence.get(tokEnd+2).get(CoreAnnotations.PartOfSpeechAnnotation.class);
            posindex = tagToId.get(pos_plus_2);
            if (posindex != null) 
                inst.setValue(161+tagToId.get(pos_plus_2),1.0);
        }
        // Feature 201 is the class, which is not specified. 
        inst.setDataset(dataTemplate);  // Associate the feature vector with the feature template.
        return inst;
    }

}
