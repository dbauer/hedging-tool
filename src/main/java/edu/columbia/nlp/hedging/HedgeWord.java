package edu.columbia.nlp.hedging;

/**
 * A possible hedge word in the hedge word dictionary.
 */ 
public class HedgeWord {

    public enum htype {
        HPROP, HREL
    }

    public String lemma;
    public htype type;
    public int id;

    public HedgeWord(String theWord, htype theType, int theId){
        lemma = theWord;
        type = theType;
        id = theId;  // ID for this hedge word so we can generate the right "word" feature 
    }


    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("<hWord: ");
        builder.append(lemma);
        builder.append(", ");
        builder.append(type == htype.HPROP ? "hProp" : "hRel");
        builder.append(">");
        return builder.toString();
    }
}
