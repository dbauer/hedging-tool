package edu.columbia.nlp.hedging;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.Reader;
import java.util.Properties;
import java.util.List;


import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.pipeline.Annotation;

/**
 * Uses the stanford CoreNLP package to preprocess documents, including 
 * tokenization, sentence splitting, and POS tagging.
 * Warning: This currently processes entire files. For very big files it would
 * be better to lazily return one sentence at a time. This does not seem to be 
 * supported by the StanfordCoreNLP high level API. 
 */ 
public class Preprocessor {

    protected StanfordCoreNLP pipeline;

    public Preprocessor() {
        Properties props;
        props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos");
        pipeline = new StanfordCoreNLP(props);
    }

    /**
     * Helper method to read an entire file into a string. 
     */
    public static String readFromStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder(512);
        Reader r = new BufferedReader(new InputStreamReader(is));
        int c = 0;
        while ((c = r.read()) != -1) {
            sb.append((char) c);
        }
        return sb.toString();
    }

    /**
     * Preprocess the reader object and return a list of lists of core labels. 
     */
    public List<CoreMap> preprocess(InputStream document) throws IOException{
        Annotation annotation= new Annotation(readFromStream(document));
        pipeline.annotate(annotation);
        return annotation.get(CoreAnnotations.SentencesAnnotation.class);
    }
}
