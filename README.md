# README for the Hedge Detector tool#

## What is this tool?

**TODO**: Anna/Julia should write a short description.

## Downloading and Installing the Software

### Requirements

The software requires Java 7 and the [Apache Maven](http://maven.apache.org/) build tool. 

### Dependencies

 * [JCommander](http://jcommander.org/) command line parser.
 * [OpenCSV](http://opencsv.sourceforge.net/)
 * The [Weka](http://www.cs.waikato.ac.nz/ml/weka/) machine learning toolkit.
 * [Stanford CoreNLP](http://nlp.stanford.edu/software/corenlp.shtml). Model files for the English POS tagger are also required.

When Maven is used to build the software, it will download dependencies automatically. 

### Checking Out Sources
Using Git:    `git clone git@bitbucket.org:dbauer/hedging-tool.git`

### Compiling and Building
Compile Using Maven:    
`cd hedging-tool`    
`mvn compile`

### Building binary JAR files
Running  `mvn package` will produces the following .jar files. Both files are fairly large because they contain a copy of the required Stanford CoreNLP POS model. 

* `hedging-tool/target/hedge-detector-portable.jar` Includes all unpacked dependencies. This file should work on any machine with Java 7. 
* `hedging-tool/target/hedge-detector.jar` Includes only class files for the tool itself. The dependencies listed above need to be in the CLASSPATH to run this file (with the exception of the POS model).

### Running the Software
 `java -jar hedge-detector-portable.jar [input doc1] [input doc2]...`

This will run the hedge detector on the input documents doc1, doc2...  and will write the output to stdout. If no input files are specified, the input is read from stdin. **IMPORTANT:** Don't try this with very large input files. Currently each input document is read and pre-processed completely before hedges are identified.

**Input format:**  The expected input are UTF-8 encoded plain text files. The hedge detector tool takes care of tokenization and sentence splitting. 

**Output format:** One sentence per line, tokenized (but not normalized), with in-line XML-esque annotations. The hedge detector annotates all potential hedge words (according to a hedge word dictionary). It labels the prediction for each occurence and the classifier confidence as attributes. **Example sentence:**  

 `And we <hedgeWord type="hRel" prediction="true" score="0.6376"> believe </hedgeWord> , in fact , that the other portions of this statute shed <hedgeWord type="hProp" prediction="false" score="0.5937"> some </hedgeWord> light onto why the residual clause is unconstitutionally vague .`

In this sentence only  *believe* is recognized as indicating a hedge .

### Changing the Hedge Word Dictionary and Classifier 

The hedge word dictionary and Weka classifier are in `hedge-detector/src/main/resources/hedgewords.041715.csv`  
and `hedge-detector/src/main/resources/ADTreeModel.062015.model`.  
The source code references these files in `HedgePredictor.java` and `WekaClassifier.java`. 

The .model file contains both a classifier and a model. The default classifier uses an Alternating Decision Tree. You can use the Weka explorer GUI to train and export new classifiers. 

It is also possible to pass a classifier or hedge word file to the software as command line arguments: 

`java -jar hedge-detector-portable.jar  -hwords [hedgewords]  -model [modelfile] [input docs...]`

